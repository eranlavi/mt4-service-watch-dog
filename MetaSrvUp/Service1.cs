﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;
using System.Threading;

namespace MetaSrvUp
{
    public partial class Service1 : ServiceBase
    {
        private string src = "mtsrv";// "STacSV";
        private System.Timers.Timer tmr = new System.Timers.Timer();

        public Service1()
        {
            InitializeComponent();

            tmr.Interval = 5000;
            tmr.Elapsed += new ElapsedEventHandler(tmr_Elapsed);
        }

        void tmr_Elapsed(object sender, ElapsedEventArgs e)
        {
            tmr.Enabled = false;

            try
            {
                ServiceController[] scServices;
                scServices = ServiceController.GetServices();
                
                bool bFound = false;
                foreach (ServiceController scTemp in scServices)
                {
                    if (scTemp.ServiceName == src)
                    {
                        bFound = true;

                        using (ServiceController sc = new ServiceController())
                        {
                            //sc.ServiceName = src;
                            sc.DisplayName = "MetaTrader 4 Server";// "Audio Service";
                            //ServiceController sc = scTemp;
                            if (sc.Status == ServiceControllerStatus.Stopped)
                            {
                                //System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", DateTime.Now.ToString() + " The service: " + src + " is in stop state. Restarting..." + "\r\n");
                                sc.Start();
                                //System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", "1\r\n");
                                sc.WaitForStatus(ServiceControllerStatus.Running, new TimeSpan(0, 0, 15));
                                //System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", "2\r\n");

                                sc.Refresh();
                                if (sc.Status != ServiceControllerStatus.Running)
                                {
                                    Log("The service: " + src + " failed to start");
                                    //System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", DateTime.Now.ToString() + " The service: " + src + " failed to start" + "\r\n");
                                }
                            }
                        }

                        break;
                    }
                }

                if (!bFound)
                {
                    Log("Could not find service: " + src);
                    //System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", DateTime.Now.ToString() + " Could not find service: " + src + "\r\n");
                }
            }
            catch (Exception ex)
            {
                Log("Exception: " + ex.Message);
                //System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", DateTime.Now.ToString() + " Exception: " + ex.Message + "\r\n");
            }

            tmr.Enabled = true;
        }

        private void Log(string txt)
        {
            try
            {
                System.IO.File.AppendAllText(@"c:\temp\metasrvup.txt", DateTime.Now.ToString() + " " + txt + "\r\n");
            }
            catch
            {

            }
        }

        protected override void OnStart(string[] args)
        {
            tmr.Enabled = true;
        }

        protected override void OnStop()
        {
            tmr.Enabled = false;
        }
    }
}
